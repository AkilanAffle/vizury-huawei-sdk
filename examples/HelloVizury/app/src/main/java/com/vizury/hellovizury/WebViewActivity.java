package com.vizury.hellovizury;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;



import com.vizury.mobile.Utils;

public class WebViewActivity extends AppCompatActivity {
    private WebView webView;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WebView webView = (WebView)findViewById(R.id.webView);
        webView.clearCache(true);
        webView.setWebViewClient(new WebViewClient());
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        //set min sdk version to 21 for accepting third party cookie
        CookieManager.getInstance().acceptThirdPartyCookies(webView);
        CookieManager.getInstance().setAcceptThirdPartyCookies(webView,true);
        webView.loadUrl("https://www.google.com");
        String cookieUrl = Utils.getInstance(getApplicationContext()).getCookieUrl();
        // https://www.google.com -> your domain where key has to be stored
        CookieManager.getInstance().setCookie("https://www.google.com","cookieUrl="+cookieUrl);

    }
    public void onBackPressed(){
        if (webView.canGoBack()){
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
